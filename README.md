## React project and AWS API Gateway using Lambda functions##

During the migration procces from requirejs to React, I created this React app which uses the facebook incubator lib. Some highlights:

> 1. It includes webpack, babel, ESLint, etc.
> 2. I'm using AWS Lambda functions based in nodejs to connect and get data from a Postgresql database using authentication from Auth0.
> 3. I'm publishing a new stage of the LookNORTH API using AWS and then showing it by using [Swagger][swagger], which is a fantastic tool for APIs.
> 4. This project is part of the a new LookNORTH release, which will happen ending 2017. In the meantime you can take a look at the [dev release][dev]

No configuration or complicated folder structures, just the files you need to build your app.
Once the installation is done, you can run some commands inside the project folder:

'''npm start''' or '''yarn start'''

Runs the app in development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.

Also, you can test it going to http://api.looknorthservices.com and typing the tablename '''Infrastructure'''. You should get **9104** records.

[swagger]: https://swaggerhub.com
[dev]: http://api.looknorthservices.com