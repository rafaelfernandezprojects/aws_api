import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SwaggerUI, { presets } from 'swagger-ui-dist/swagger-ui-bundle';
import 'swagger-ui-dist/swagger-ui.css';
import './App.css';

class App extends Component {

  componentDidMount() {
      SwaggerUI({
          dom_id: '#swaggerContainer',
          url: this.props.url,
          spec: this.props.spec,
          presets: [presets.apis]
      });
  }

  render() {
    return (
      <div className="container">
        <div id="swaggerContainer" ></div>
      </div>
    );
  }
}

App.propTypes = {
    url: PropTypes.string,
    spec: PropTypes.object
};

App.defaultProps = {
    url: "https://s3-us-west-2.amazonaws.com/api.looknorthservices.com/coresight.json"
};

export default App;
